#!/bin/bash

apt-get update
apt-get -y  install sudo curl ca-certificates apt-transport-https software-properties-common libwebp-dev unzip
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update

for x in $(curl https://www.cloudflare.com/ips-v4); do iptables -A INPUT -p tcp -m multiport --dports https -s $x -j ACCEPT; done
for x in $(curl https://www.cloudflare.com/ips-v6); do ip6tables -A INPUT -p tcp -m multiport --dports https -s $x -j ACCEPT; done
iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -s 127.0.0.1 -j ACCEPT 	
iptables -A INPUT -s 88.99.6.44 -j ACCEPT 	
iptables -A INPUT -s 81.30.161.141 -j ACCEPT
iptables -A INPUT -s 144.76.80.103 -j ACCEPT
iptables -P OUTPUT ACCEPT 
iptables -P INPUT DROP
ip6tables -P OUTPUT ACCEPT
ip6tables -P INPUT DROP
iptables -L -v --line-numbers -n -w
ip6tables -L -v --line-numbers -n -w 


wget -q https://packages.sury.org/php/apt.gpg -O- | sudo apt-key add -
echo "deb https://packages.sury.org/php/ stretch main" | sudo tee /etc/apt/sources.list.d/php.list
apt-get update
PACKAGES="php7.1-curl php7.1-mbstring php7.1-mysql php7.1-gd php7.1-xml php7.1 nginx php7.1-fpm php-memcache memcached git iptables-persistent"
sudo apt-get -y --force-yes install $PACKAGES

echo "iptables OK"
echo "install OK"

mv /etc/php/7.1/fpm/pool.d/www.conf /etc/php/7.1/fpm/pool.d/www.conf_def
cp files/www.conf /etc/php/7.1/fpm/pool.d/
rm -rf /etc/nginx/sites-available/*
rm -rf /etc/nginx/sites-enabled/*
cp files/cloudflare /etc/nginx/
cp files/_default.conf /etc/nginx/conf.d/
cp files/default_ssl.conf /etc/nginx/conf.d/
rm -rf /var/www/*


ssh-keygen -t rsa -b 4096
echo
cat /root/.ssh/id_rsa.pub
echo
printf 'Put id_rsa.pub to bitbucket'
read -p "Press enter to continue"
printf 'Put name from project' 
echo
read pr_name
printf 'Put git from bitbucket' 
echo
read git
#git clone git@bitbucket.org:intary/site_superomatic.git
$git /var/www/$pr_name
#git clone git@bitbucket.org:intary/depth_exchange_admin.git /var/www/depth_exchange_admin

mkdir /etc/nginx/ssl
openssl req -out /etc/nginx/ssl/server.csr -new -newkey rsa:2048 -nodes -keyout /etc/nginx/ssl/server.key -subj '/C=RU/ST=Ivanovo/L=Ivanovo/O=Guru Project/OU=Research team/emailAddress=root@primer.com'
openssl x509 -req -in /etc/nginx/ssl/server.csr -signkey /etc/nginx/ssl/server.key -out /etc/nginx/ssl/server.crt
#read -p "Enter your name of project: " st_name
#sed -i "s/set $sathost \"site_goldslot\";/set $sathost \"${st_name}\";/"  ~/Documents/WORK/Ngnix/Nginx_php-fpm/default_ssl.conf
#read -p "Enter your name of project: " n


sed -i "s/set \$sathost \"site_goldslot\";/set \$sathost \"$pr_name\";/"  /etc/nginx/conf.d/default_ssl.conf
#sed -i "s/set \$sathost \"site_goldslot\";/set \$sathost \"depth_exchange_admin\";/"  /etc/nginx/conf.d/default_ssl.conf


cp files/deployment.php /var/www/$pr_name/

printf 'Put password for deployment' 
echo
read pass
sed -i "s/\$password = \"password\";/\$password = \"$pass\";/"  /var/www/$pr_name/deployment.php
#sed -i "s/\$password = \"password\";/\$password = \"$pass\";/"  /var/www/$pr_name/deployment.php
#$password = "password";

nginx -t
service nginx restart
echo "nginx OK"
php-fpm7.1 -t
sudo service php7.1-fpm restart
echo "php-fpm7.1 OK"
mv /etc/security/limits.conf /etc/security/limits.confdef
cp files/limits.conf /etc/security/
echo "limits OK"
mv /etc/sudoers  /etc/sudoersdef
cp files/sudoers /etc/

read -p "Press enter to continue"

curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
chmod +x /usr/local/bin/composer
composer -V
if [ -f /var/www/$pr_name/composer.json ]; then
    echo "File composer.json found!"
    cd /var/www/$pr_name/ && composer install
fi

printf 'reboot'

