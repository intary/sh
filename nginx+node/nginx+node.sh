#!/bin/bash

apt-get update
apt-get -y  install sudo curl ca-certificates apt-transport-https software-properties-common libwebp-dev unzip
sudo apt-get update
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCoVqY6emobBZJMVDoTDqJoOFhY8TpStat+XbpKeeeIlkhViI3QeVyrGJVYn8LpqW6RbqfvntqXgjUJ3uDbw+mj7IpSDZBMHUD/N12KD+ddPbo16EnEpexzDgKWfOQ/vuNBRx6zYo4ncs8FULJ91V7X97swIhQmDQXZymgAazUNt7EMhPzcgsG0j0R8qWHS/SpBmiP9IVtliVqHOPfbLVH+NzEYQ1onrX3vwiTUX1PRoqTkaFHNQGYoVEDEAVw0hU730fRyjiB6QFl7367PCk1z6HeRzGoheATOIRwAMJZHm9tnntLvAsLIusihRHOBwDQLIIb5XMXnAAp/LyA4SH7dl8vuUFfxXcSVYYxaVHnwA7tcACxt5T7vDtwTNBUUDaxDX/jZ6XaX7QXRZ/iNhae68umxBHvUQ2HvDXmu4wOhR7wOAEA/d3uDnCU3At2S1m68KE5sKY8ALAWQ9wnqqYYFsL3MHMdkUDMvmOLtftuy6/FhOS6e5rll5qJn3C+SwDZMZxUWcpMYjxzmmgwIsXcReulBaaeZaXHNYB3BmJzBC+dHVV+Wo+IIgTiWblG3reaiC+jLF+/g/GJlwyFbWa0xSBa6b5Zq+8Iua1TZIef0ACmbXpoBIzZvCvkihOfi4Kl205Ex+xUvrziHmZHIumGbR0fMKJptddWgh3HyD9Ik/w== root@Dmitro_Yavorskiy-desktop
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCgbq956yyA4h8Kmd7LSBh00l0G2Glc+USjuIbUEOl95HA1V+u/uDK9dsQ7HoCTSbUcXD+Yb7rOSUUE+dQloWmqwnDAXhaGgdeSjo75z9WU5/gKHEzCCjNFfCSUta+8XjNoW8NxeFY2m5q/yN0KbuzYXbyIhpKGHoln1PBoYShWDasMYBOBwY3UMTR/IbATPHdMDD6Khqm4iRYZBRIO8jbs3Nq3o+83UX8iB0O/+N3OPR/2ZerjSnLrkgm6H/0uGnk0J558lY+Hd9DfXXb/9X4U7/LOm6HLaAVL7/A2BzhoWSE8byvw7Kp5gsjjFyj2NhBQIOyzncvo943FyGSnZUx7 saloprj@gmail.com
 " >> /root/.ssh/authorized_keys

for x in $(curl https://www.cloudflare.com/ips-v4); do iptables -A INPUT -p tcp -m multiport --dports https -s $x -j ACCEPT; done
for x in $(curl https://www.cloudflare.com/ips-v6); do ip6tables -A INPUT -p tcp -m multiport --dports https -s $x -j ACCEPT; done
iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -s 127.0.0.1 -j ACCEPT 	
iptables -A INPUT -s 88.99.6.44 -j ACCEPT 	
iptables -A INPUT -s 81.30.161.141 -j ACCEPT
iptables -A INPUT -s 144.76.80.103 -j ACCEPT
iptables -P OUTPUT ACCEPT 
iptables -P INPUT DROP
ip6tables -P OUTPUT ACCEPT
ip6tables -P INPUT DROP
iptables -L -v --line-numbers -n -w
ip6tables -L -v --line-numbers -n -w 

sudo apt-get install curl python-software-properties
curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -

PACKAGES="nginx git iptables-persistent nodejs"
sudo apt-get -y --force-yes install $PACKAGES

node -v 

rm -rf /etc/nginx/sites-available/*
rm -rf /etc/nginx/sites-enabled/*
cp files/de_hosts /etc/nginx/sites-enabled
mkdir -p /var/log/de/
tar -zxvf files/de_configuration.tar.gz -C /var/www/
echo "create" >/var/log/de/hosts-errors.log
cp files/configurations.zip /var/www/

cd /var/www/
unzip configurations.zip 
rm configurations.zip 
service nginx restart